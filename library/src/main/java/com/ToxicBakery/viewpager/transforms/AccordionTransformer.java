/*
 * Copyright 2014 Toxic Bakery
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ToxicBakery.viewpager.transforms;

import android.graphics.Color;
import android.util.Log;
import android.view.View;

public class AccordionTransformer extends ABaseTransformer {

    @Override
    protected void onTransform(View view, float position) {
        int[] loc = new int[2];
        view.getLocationOnScreen(loc);
        int x =loc [0];
        int y = loc[1];
        view.setPivotX(position < 0 ? 0 : view.getWidth());
//        view.setPivotX(view.getWidth()/2 + view.getLeft());
        view.setScaleX(position < 0 ? 1f + position : 1f - position);

        Log.d("aniamtiontag","555  "+view.getLeft()+":"+view.getX()+":"+loc[0]);

//        if (position < 0) {
//            Log.d("scalex","ofg "+view.getScaleX()+":  "+position);
//            view.setTranslationX((position * view.getScaleX())/2);
//        }
//        if (position <= 0) {
//            view.setTranslationX(view.getWidth() / 16 * position);
////            view.setPivotX(-view.getWidth()*position);
////            view.setScaleX((1f + position) * 1);
//            if (position <= -0.5f) {
//                view.setPivotX(-view.getWidth()*position);
//                view.setScaleX((1f + position) * 2);
//                view.setBackgroundColor(Color.GREEN);
//            }
//            else {
//                view.setBackgroundColor(Color.BLUE);
//            }
//        } else if (position > 0 && position <= 1) {
//            view.setTranslationX(-view.getWidth() / 16 * position);
////            view.setPivotX(view.getWidth() * 1*position);
////            view.setScaleX((1f - position) * 1);
//            if (position >= 0.5) {
//                view.setPivotX(view.getWidth() * 1*position);
//                view.setScaleX((1f - position) * 2);
//                view.setBackgroundColor(Color.YELLOW);
//            }
//            else
//                view.setBackgroundColor(Color.GRAY);
//            Log.d("animationplus",view.getX()+":"+position);
//        }


//        if (position <= 0) {
//            view.setTranslationX(0);
//        } else if (position > 0 && position <= 1) {
//            view.setTranslationX(-view.getWidth() / 1 * position);
//        }
    }

}
